# check_perfops_cli

[PerfOps CLI](https://perfops.net/cli) Naemon/Icinga/Nagios plugin which uses the [API](https://docs.perfops.net/) for checks.

[[_TOC_]]

## Requirements
* Ruby >2.5

* PerfOps API token

### Options
* -s: Target address

* -t: PerfOps API token

* -l: PerfOps node location - https://api.perfops.net/node

* -m: Check mode to use

* -a: Expected IP/HOST the DNS server should return (regex)

* --dns-server: DNS Server to use

* --dns-query-type: Specify DNS query type (A,MX,CNAME,...)

* -w: Warning threshold

* -c: Critical threshold

## Modes
### Ping
Run a ping test.

Threshold format for -w and -c is 200.25,60% for 200.25 msec RTA and 60% packet loss.

```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m ping -l asia -s ftp.example.com -w 100,2 -c 200,10
Critical - Changhua City - ftp.example.com pl: 0%, rta: 277.050ms | pl=0%;2;10 rta=277.050ms;100;200
```

### Latency
Run an ICMP test to get the latency.

```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m latency -l austria -s mail.example.com -w 10 -c 20
OK - Vienna - mail.example.com 0.608ms | time=0.608ms;20;50
```

### DNS resolve
Resolves a DNS record and checks for the expected IP/HOST in the response.
You can use 127.0.0.1 for `--dns-server` to use the local resolver.

#### A
```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m dns-resolve --dns-server 8.8.8.8 -l austria -a 1.1.1.1 -s www.example.com --dns-query-type A
Warning - Vienna - www.example.com A 1.1.1.1 not found, result 1.1.1.2
```

#### MX
```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m dns-resolve --dns-server 8.8.8.8 -l austria -a mail.example.com -s test.at --dns-query-type MX
OK - Vienna - example.com MX 5 mail.example.com. found
```

#### CNAME
```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m dns-resolve --dns-server 8.8.8.8 -l austria -a www -s ftp.example.com --dns-query-type CNAME
OK - Vienna - ftp.example.com CNAME www.example.com. found
```

### DNS perf
Find the time it took to resolve a DNS query.

```shell
naemon@server:plugins$ ./check_perfops_cli.rb -t <API_TOKEN> -m dns-perf --dns-server 8.8.8.8 -l "north america" -s mail.example.com -w 10 -c 20
Critical - Los Angeles - mail.example.com DNS query took 136ms | time=136ms;10;20
```
