#!/usr/bin/env ruby
# frozen_string_literal: true

#
# PerfOps CLI Plugin
# ==
# Author: Marco Peterseil
# Created: 03-2020
# License: GPLv3 - http://www.gnu.org/licenses
# URL: https://gitlab.com/6uellerBpanda/check_perfops_cli
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require 'optparse'
require 'net/https'
require 'json'
require 'date'

version = 'v0.1.0'

# optparser
banner = <<~HEREDOC
  check_perfops_cli #{version} [https://gitlab.com/6uellerBpanda/check_perfops_cli]\n
  This plugin checks various parameters of PerfOps CLI via API\n
  Mode:
    ping                      Run a ping test
    latency                   Run an ICMP test to get the latency
    dns-resolve               Resolves a DNS record and checks for the expected IP/HOST in the response
    dns-perf                  Find the time it took to resolve a DNS query

  Usage: #{File.basename(__FILE__)} [options]
HEREDOC

options = {}
OptionParser.new do |opts| # rubocop:disable  Metrics/BlockLength
  opts.banner = banner.to_s
  opts.summary_width = 60
  opts.separator ''
  opts.separator 'Options:'
  opts.on('-s', '--address ADDRESS', 'Target address') do |s|
    options[:address] = s
  end
  opts.on('-t', '--token TOKEN', 'API token') do |t|
    options[:token] = t
  end
  opts.on('-l', '--location LOCATION', 'Location name') do |l|
    options[:location] = l
  end
  opts.on('-m', '--mode MODE', 'Mode to check') do |m|
    options[:mode] = m
  end
  opts.on('-a', '--expected-address EXPECTED_ADDRESS', 'Expected IP/HOST the DNS server should return') do |a|
    options[:expected_address] = a
  end
  opts.on('--dns-server DNS_SERVER', 'DNS server to use to query') do |dns_server|
    options[:dns_server] = dns_server
  end
  opts.on('--dns-query-type DNS_QUERY_TYPE', 'DNS query type') do |dns_query_type|
    options[:dns_query_type] = dns_query_type
  end
  opts.on('-w', '--warning WARNING', 'Warning threshold') do |w|
    options[:warning] = w
  end
  opts.on('-c', '--critical CRITICAL', 'Critical threshold') do |c|
    options[:critical] = c
  end
  opts.on('-v', '--version', 'Print version information') do
    puts "check_perfops_cli #{version}"
  end
  opts.on('-h', '--help', 'Show this help message') do
    puts opts
  end
  ARGV.push('-h') if ARGV.empty?
end.parse!

# check args for api calls
unless ARGV.empty?
  raise OptionParser::MissingArgument if options[:address].nil? || options[:token].nil?
end

# check perfops cli
class CheckPerfopsCli
  def initialize(options)
    @options = options
    init_arr
    ping_check
    latency_check
    dns_resolve_check
    dns_perf_check
  end

  #--------#
  # HELPER #
  #--------#

  def init_arr
    @perfdata = []
    @message = []
    @critical = []
    @warning = []
    @okays = []
  end

  # define some helper methods for naemon
  def ok_msg(message)
    puts "OK - #{message}" + @debug.to_s
    exit 0
  end

  def crit_msg(message)
    puts "Critical - #{message}" + @debug.to_s
    exit 2
  end

  def warn_msg(message)
    puts "Warning - #{message}" + @debug.to_s
    exit 1
  end

  def unk_msg(message)
    puts "Unknown - #{message}"
    exit 3
  end

  def build_perfdata(*args, perfdata:) # rubocop:disable Metrics/AbcSize
    if args.any?
      @perfdata << "#{args[0]};#{@options[:warning].split(',')[1].to_i};#{@options[:critical].split(',')[1].to_i} #{perfdata};#{@options[:warning].split(',')[0].to_i};#{@options[:critical].split(',')[0].to_i}" # rubocop:disable Layout/LineLength
    else
      @perfdata << "#{perfdata};#{@options[:warning]};#{@options[:critical]}"
    end
  end

  # build service output
  def build_output(msg:)
    @message = msg
  end

  # helper for threshold checking
  def check_thresholds_ping(pl:, rta:) # rubocop:disable Naming/MethodParameterName
    if rta > @options[:critical].split(',')[0].to_i || pl > @options[:critical].split(',')[1].to_i
      @critical << @message
    elsif rta > @options[:warning].split(',')[0].to_i || pl > @options[:warning].split(',')[1].to_i
      @warning << @message
    else
      @okays << @message
    end
    # make the final step
    build_final_output
  end

  def check_thresholds(data:)
    if data > @options[:critical].to_i
      @critical << @message
    elsif data > @options[:warning].to_i
      @warning << @message
    else
      @okays << @message
    end
    # make the final step
    build_final_output
  end

  # mix everything together for exit
  def build_final_output
    perf_output = " | #{@perfdata.join(' ')}"
    if @critical.any?
      crit_msg(@critical.join(', ') + perf_output)
    elsif @warning.any?
      warn_msg(@warning.join(', ') + perf_output)
    else
      ok_msg(@okays.join(', ') + perf_output)
    end
  end

  def fetch_id
    http_connect(req: 'post')
    data = JSON.parse(@response.body)
    @id = data['id']
    unk_msg("#{data.keys[0]}: #{data.values[0]}") unless @id # bail out when no nodes are online
  end

  def fetch_data
    fetch_id
    sleep 5 # wait till test is finished
    http_connect(path: "#{@options[:mode]}/#{JSON.parse(@response.body)['id']}")
    test_unreachable(data: JSON.parse(@response.body)['items'][0]['result'])
  end

  def test_unreachable(data:)
    case data['output']
    when /0\sreceived,\s100%\spacket\sloss/ then crit_msg("#{data['node']['city']} - #{@options[:address]} no packets received")
    when /empty\sresponse/, /unknown\shost/ then crit_msg("#{data['node']['city']} - #{@options[:address]} unknown host")
    end
  end

  #----------#
  # API AUTH #
  #----------#

  # create url
  def url(path:, req: 'get') # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    uri = URI("https://api.perfops.net/run/#{path}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    if req == 'post'
      request = Net::HTTP::Post.new(uri.request_uri)
      request['Content-Type'] = 'application/json'
      request['Authorization'] = @options[:token]
      request.body = {
        'target' => @options[:address].to_s,
        'location' => @options[:location].to_s,
        'dnsServer' => @options[:dns_server].to_s,
        'param' => @options[:dns_query_type],
        'limit' => '1'
      }.compact.to_json
    else
      request = Net::HTTP::Get.new(uri.request_uri)
    end
    @response = http.request(request)
  rescue StandardError => e
    unk_msg(e)
  end

  # init http req
  def http_connect(path: @options[:mode], req: 'get')
    url(path: path, req: req)
    check_http_response
  end

  # check http response
  def check_http_response
    unk_msg(@response.message).to_s if @response.code.is_a? Net::HTTPError
  end

  #--------#
  # CHECKS #
  #--------#

  ###--- PING CHECK ---###
  def ping_check # rubocop:disable Metrics/AbcSize
    return unless @options[:mode] == 'ping'
    fetch_data
    data = JSON.parse(@response.body)['items'][0]['result']
    pl = data['output'].match(/.*?(\d+%)\spacket\sloss/)[1].chomp('%')
    rta = data['output'].match(%r{/.*?=\s[^/]*/([^/]*)})[1]
    build_output(msg: "#{data['node']['city']} - #{@options[:address]} pl: #{pl}%, rta: #{rta}ms")
    build_perfdata("pl=#{pl}%", perfdata: "rta=#{rta}ms")
    check_thresholds_ping(pl: pl.to_i, rta: rta.to_i)
  end

  ###--- LATENCY CHECK ---###
  def latency_check
    return unless @options[:mode] == 'latency'
    fetch_data
    data = JSON.parse(@response.body)
    build_output(msg: "#{data['items'][0]['result']['node']['city']} - #{@options[:address]} #{data['elapsedTime']}ms")
    build_perfdata(perfdata: "time=#{data['elapsedTime']}ms")
    check_thresholds(data: data['elapsedTime'].to_i)
  end

  ###--- DNS RESOLVE CHECK ---###
  def dns_resolve_check
    return unless @options[:mode] == 'dns-resolve'
    fetch_data
    data = JSON.parse(@response.body)['items'][0]['result']
    output = [*data['output']][0]
    pretext = "#{data['node']['city']} - #{@options[:address]} #{@options[:dns_query_type]}"
    if output.match(@options[:expected_address].to_s)
      ok_msg("#{pretext} #{output} found")
    else
      warn_msg("#{pretext} #{@options[:expected_address]} not found, result #{output}")
    end
  end

  ###--- DNS PERF CHECK ---###
  def dns_perf_check
    return unless @options[:mode] == 'dns-perf'
    fetch_data
    data = JSON.parse(@response.body)['items'][0]['result']
    build_output(msg: "#{data['node']['city']} - #{@options[:address]} DNS query took #{data['output']}ms")
    build_perfdata(perfdata: "time=#{data['output']}ms")
    check_thresholds(data: data['output'].to_i)
  end
end

CheckPerfopsCli.new(options)
